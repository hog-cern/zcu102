#------------------------------------------------------------------------------
#  (c) Copyright 2013-2015 Xilinx, Inc. All rights reserved.
#
#  This file contains confidential and proprietary information
#  of Xilinx, Inc. and is protected under U.S. and
#  international copyright and other intellectual property
#  laws.
#
#  DISCLAIMER
#  This disclaimer is not a license and does not grant any
#  rights to the materials distributed herewith. Except as
#  otherwise provided in a valid license issued to you by
#  Xilinx, and to the maximum extent permitted by applicable
#  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
#  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
#  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
#  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
#  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
#  (2) Xilinx shall not be liable (whether in contract or tort,
#  including negligence, or under any other theory of
#  liability) for any loss or damage of any kind or nature
#  related to, arising under or in connection with these
#  materials, including for any direct, or any indirect,
#  special, incidental, or consequential loss or damage
#  (including loss of data, profits, goodwill, or any type of
#  loss or damage suffered as a result of any action brought
#  by a third party) even if such damage or loss was
#  reasonably foreseeable or Xilinx had been advised of the
#  possibility of the same.
#
#  CRITICAL APPLICATIONS
#  Xilinx products are not designed or intended to be fail-
#  safe, or for use in any application requiring fail-safe
#  performance, such as life-support or safety devices or
#  systems, Class III medical devices, nuclear facilities,
#  applications related to the deployment of airbags, or any
#  other applications that could lead to death, personal
#  injury, or severe property or environmental damage
#  (individually and collectively, "Critical
#  Applications"). Customer assumes the sole risk and
#  liability of any use of Xilinx products in Critical
#  Applications, subject only to applicable laws and
#  regulations governing limitations on product liability.
#
#  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
#  PART OF THIS FILE AT ALL TIMES.
#------------------------------------------------------------------------------


# UltraScale FPGAs Transceivers Wizard IP example design-level XDC file
# ----------------------------------------------------------------------------------------------------------------------

# Location constraints for differential reference clock buffers
# Note: the IP core-level XDC constrains the transceiver channel data pin locations
# ----------------------------------------------------------------------------------------------------------------------
set_property PACKAGE_PIN L7 [get_ports mgtrefclk0_x1y1_n]
set_property PACKAGE_PIN L8 [get_ports mgtrefclk0_x1y1_p]

# Location constraints for other example design top-level ports
# Note: uncomment the following set_property constraints and replace "<>" with appropriate pin locations for your board
# ----------------------------------------------------------------------------------------------------------------------
#Following replaced by differential buffer
#set_property package_pin <> [get_ports hb_gtwiz_reset_clk_freerun_in]
#set_property iostandard  <> [get_ports hb_gtwiz_reset_clk_freerun_in]

#Following two pins are connected to switches
#centre button sw15
set_property package_pin AG13 [get_ports hb_gtwiz_reset_all_in]
set_property IOSTANDARD LVCMOS33 [get_ports hb_gtwiz_reset_all_in]
#east button sw17
set_property PACKAGE_PIN AE14 [get_ports link_down_latched_reset_in]
set_property IOSTANDARD LVCMOS33 [get_ports link_down_latched_reset_in]

# Follwing two pins AG14 and AF13 are LED drivers
#led 0 , ds38
set_property PACKAGE_PIN AG14 [get_ports blinken_led]
set_property IOSTANDARD LVCMOS33 [get_ports blinken_led]
#led 1 , ds37
set_property PACKAGE_PIN AF13 [get_ports link_status_out]
set_property IOSTANDARD LVCMOS33 [get_ports link_status_out]
#led 2, ds39
set_property PACKAGE_PIN AE13 [get_ports link_down_latched_out]
set_property IOSTANDARD LVCMOS33 [get_ports link_down_latched_out]



# Clock constraints for clocks provided as inputs to the core
# Note: the IP core-level XDC constrains clocks produced by the core, which drive user clocks via helper blocks
# ----------------------------------------------------------------------------------------------------------------------
#create_clock -name clk_freerun -period 4.166 [get_ports hb_gtwiz_reset_clk_freerun_in]
set_property PACKAGE_PIN AL8 [get_ports sysclk_p]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports sysclk_p]
#set_property DIFF_TERM FALSE [get_ports sysclk_p]

create_clock -period 4.166 -name clk_freerun [get_ports sysclk_p]


create_clock -period 6.400 -name clk_mgtrefclk0_x1y1_p [get_ports mgtrefclk0_x1y1_p]

# False path constraints
# ----------------------------------------------------------------------------------------------------------------------
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
##set_false_path -to [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_*_reg}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_meta_reg/D}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_meta_reg/PRE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync1_reg/PRE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync2_reg/PRE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_sync3_reg/PRE}]
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_out_reg/PRE}]


set_false_path -to [get_cells -hierarchical -filter {NAME =~ *gtwiz_userclk_tx_inst/*gtwiz_userclk_tx_active_*_reg}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *gtwiz_userclk_rx_inst/*gtwiz_userclk_rx_active_*_reg}]




create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list {mgt/mgt/example_wrapper_inst/gtwiz_userclk_tx_inst/gtwiz_userclk_tx_usrclk_out[0]}]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {mgt_rx_data[0]} {mgt_rx_data[1]} {mgt_rx_data[2]} {mgt_rx_data[3]} {mgt_rx_data[4]} {mgt_rx_data[5]} {mgt_rx_data[6]} {mgt_rx_data[7]} {mgt_rx_data[8]} {mgt_rx_data[9]} {mgt_rx_data[10]} {mgt_rx_data[11]} {mgt_rx_data[12]} {mgt_rx_data[13]} {mgt_rx_data[14]} {mgt_rx_data[15]} {mgt_rx_data[16]} {mgt_rx_data[17]} {mgt_rx_data[18]} {mgt_rx_data[19]} {mgt_rx_data[20]} {mgt_rx_data[21]} {mgt_rx_data[22]} {mgt_rx_data[23]} {mgt_rx_data[24]} {mgt_rx_data[25]} {mgt_rx_data[26]} {mgt_rx_data[27]} {mgt_rx_data[28]} {mgt_rx_data[29]} {mgt_rx_data[30]} {mgt_rx_data[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 8 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {mgt_rx_ctrl[0]} {mgt_rx_ctrl[1]} {mgt_rx_ctrl[2]} {mgt_rx_ctrl[3]} {mgt_rx_ctrl[4]} {mgt_rx_ctrl[5]} {mgt_rx_ctrl[6]} {mgt_rx_ctrl[7]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
