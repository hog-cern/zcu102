# zcu102
1. Clone the repository with the `--recursive` option: `git clone --recursive git@gitlab.com:hog-cern/zcu102.git`
2. To update the repository run `git pull` and `git submodule update`
3. Run `./Hog/CreateProject.sh fmc0`
4. The project is created in the `Projects` directory
5. Optionally, to build the project from command line run `./Hog/LaunchWorkflow.sh fmc0`

# ZCU102 Data Generator Instructions

## Network Connection
This is done following the [zcu102 basex](https://github.com/ipbus/ipbus-firmware/blob/master/boards/zcu102/basex/synth/firmware/hdl/top_zcu102_basex.vhd#L100) IPbus example.
The network adapter to be used is the one on the top right of the on-board cage. We only tested it with a RJ45 connector.

### IP Address
This is done as in the IPbus example mentioned above,  we only use `10.0.0.*` rather than `192.168.0.*.`.
The higher bytes of the IP address are hard coded in the firmware as `10.0.0.X`,
where `X` can go from 16 to 31 depending on the dip switch on the board (the only one with 4 switches located close to the PCIe connector).
The default position of the dip switch is all on (0xF) so the default address of the board is 10.0.0.31

## How to
### 1. RAMs
First, you have to load the data words in the data memories and the control words in the control memories.
The control words are only 4 bit, so must fit 8 words in a 32-bit word in the RAM.

The 4-bit allow the key character to be put in either of the 4 bytes of the data words.
For example you can have data =0x0000000BC with control=1 but also data=0x00BC0000 with control=4.

The data RAMs are located at the following address: 
```
dprams.data_dpram<n>
```
Where `n` goes from 0 to 7. They are 1024 words deep.

The control RAMs are located at:
```
dprams.ctrl_dpram<n>
```
Where `n` goes from 0 to 7. They are 1024/8=128 words deep, as every 32-bit word contains the control corresponding to 8 data words. 

### 2. Control Registers
There is one 32-bit register for global control, with the following bits:
```
control_1.pbf_start
control_1.mgt_reset
```
The former starts the playback functionalities logic, whereas the latter resets the mgt.
There is a 32-bit register for each channel, to control the playback. The controls are the following:
```
pbf<n>.rst      #Reset
pbf<n>.en       #Enable
pbf<n>.loop     #Infinite loop
pbf<n>.load     #Load an address offset 
pbf<n>.loadVal  #Offset to be loaded
pbf<n>.reps     #Number of repetitions (if infinite loop is not set)
```
After you have loaded all the memories, you can start the data flow by writing 1 and then 0 to the following registers:
```
control_1.mgt_reset
control_1.pbf_start
```
The data flow starts and the playback is configured via the pbf registers.

## Provided Scripts
Scripts are located in the `script` directory and arre written in python and use the [uhal](https://ipbus.web.cern.ch/ipbus/doc/user/html/software/uhalQuickTutorial.html) library.

### 1. IPbus XML Files
The address map XML files are sorted in the `xml` directory. They are needed to communicate with the board. The main one, pointing to all the others is called `xml/zcu102.xml`

### 2. List of Scripts
If `import uhal` works, you can run all the scripts with python.
Just run them without argument to read the usage.

| Script Name | Function | Arguments | 
| -------- | -------- | -------- |
| `list.py`  | General script that lists and reads all IPbus registers and RAMs | `<xml file> <ip address>` |
| `load.py`  | Load the content of a 33b file to a memory in the ZCU102. Then reset the counters and starts the data flow. |`<33bit file> <RAM number> <ip address>`|
| `read.py`  | Read the content of a RAM in the ZCU board to a 33b text file. | `<33bit file> <tx|rx> <RAM number> <ip address>` |
| `reset.py` | Reset the mgt, reset and disable the channels | `-`|
| `play.py`  | Start or stop the data flow setting the number of repetitions | `<Stop> <Reps> <Infinite Loop> <ip address>` |

Some 33bit txt files are in the script directory to be used as examples.

### 3. Examples
#### 3.1 Read From a Memory
To dump the content of a memory into a file, use the `read.py` script. From the `script` directory, run
```bash
$ python read.py 33b_read.txt tx 0 10.0.0.31
```
to read the content of the transmission memory 0 to a (possibly new) file called `33b_read.txt`.

To read a receiving RAM use the option `rx` as follows:
```bash
$ python read.py 33b_read.txt rx 7 10.0.0.31
```
Please note that tx RAM 0 will be recorded in rx RAM 7 if you are doing a loop back test, i.e. you have connected the 2 QSFP connectors with one fibre.

#### 3.2 Reset the Mgt and the Playback Channels
To stop the data flow and disable all of the channels, use the `reset.py` script. From the `script` directory, run:
```bash
$ python reset.py
```

#### 3.3 Load a File into One of the RAM
To load the content of a file into a RAM, use the `load.py` script. To load the content of the `33b.txt` file (provided as an example) into the RAM number 5 and reset the mgt, run from the `script` directory:
```bash
$ python load.py 33b.txt 5 10.0.0.31
```

#### 3.4 Start/Stop the Playback
After loading the intended memories, use the `play.py` script to start the data-flow.
This will start the data flow with an infinite loop:
```bash
$ python play.py 0 0 1 10.0.0.31
```
whereas this starts the data flow and loops over the enabled memories for four times:
```bash
$ python play.py 0 4 0 10.0.0.31
```

To stop the data flow, reset the mgt, and reset all of the playback channels, from the `script` directory, run:
```bash
$ python play.py 1 0 0 10.0.0.31
```

#### 3.5 Example of a Complete Configuration
To reset, load the `33b.txt` in the RAM `5`, start the data flow for an infinite loop, and stop the data flow, from the `script` directory, run:
```bash
$ python reset.py
$ python load.py 33b.txt 5 10.0.0.31
$ python play.py 0 0 1 10.0.0.31
$ python play.py 1 0 0 10.0.0.31
```