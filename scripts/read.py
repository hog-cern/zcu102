import sys # For sys.argv and sys.exit
import uhal

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print "usage: read.py <33bit file> <tx|rx> <RAM number> <ip address>"
        sys.exit(1)

    txt_file = sys.argv[1]
    input_output=sys.argv[2]
    n_ram =sys.argv[3]
    ip_add = sys.argv[4]

    if input_output == "tx":
        print "Reading tx RAMS"
        data_name =  "dprams_tx.data_dpram{}"
        ctrl_name =  "dprams_tx.ctrl_dpram{}"
        N=65535
    elif input_output == "rx":
        print "Reading rx RAMS"
        data_name =  "dprams_rx.rx_data{}"
        ctrl_name =  "dprams_rx.rx_ctrl{}"
        N=1024
    else:
        print "Error: RAM can be tx or rx not {}".format(input_output)
        exit

    hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://{}:50001".format(ip_add), "file://../xml/zcu102.xml")

    # Write
    mgtData = hw.getNode(data_name.format(n_ram)).readBlock(N)
    mgtCtrl = hw.getNode(ctrl_name.format(n_ram)).readBlock(N/8)
    hw.dispatch()

    f = open(txt_file, 'w') 
    count = 0
    for d in mgtData:
        nibble = count % 8
        if (mgtCtrl[count//8]//(0x10**nibble))&0xF == 0:
            ctrl = 0
        else:
            ctrl = 1
        f.write("0x{:08x} {}\n".format(d, ctrl))
        count += 1
    # write to file
    f.close()
