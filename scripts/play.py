import sys # For sys.argv and sys.exit
import uhal

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print "usage: play.py <Stop flag> <Number of repetitions> <Infinite Loop flag> <ip address>"
        sys.exit(1)

    stop_flag = int(sys.argv[1])
    reps = int(sys.argv[2])
    infinite_loop = int(sys.argv[3])
    ip_add = sys.argv[4]

    hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://{}:50001".format(ip_add), "file://../xml/zcu102.xml")
    

    #Set the control registers for the readback functionalities
    print("Enabling data reception...")
    reg = hw.getNode("registers.rbf.rst").write(0x1)
    reg = hw.getNode("registers.rbf.en").write(0x0)
    reg = hw.getNode("registers.rbf.loop").write(0x0)
    reg = hw.getNode("registers.rbf.load").write(0x0)
    reg = hw.getNode("registers.rbf.loadVal").write(0x0)
    reg = hw.getNode("registers.rbf.reps").write(0x3)
    reg = hw.getNode("registers.rbf.discard_comma").write(0x1)

    hw.dispatch()
    reg = hw.getNode("registers.rbf.rst").write(0x0)
    reg = hw.getNode("registers.rbf.en").write(0x1)
    hw.dispatch()

    #Set the number of repetions and loop for all the channels
    print("Setting infinite loop to {}".format(infinite_loop))
    print("Setting number of reps to {}".format(reps))
    reg = hw.getNode("registers.pbf0.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf1.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf2.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf3.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf4.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf5.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf6.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf7.loop").write(infinite_loop)
    reg = hw.getNode("registers.pbf0.reps").write(reps)
    reg = hw.getNode("registers.pbf1.reps").write(reps)
    reg = hw.getNode("registers.pbf2.reps").write(reps)
    reg = hw.getNode("registers.pbf3.reps").write(reps)
    reg = hw.getNode("registers.pbf4.reps").write(reps)
    reg = hw.getNode("registers.pbf5.reps").write(reps)
    reg = hw.getNode("registers.pbf6.reps").write(reps)
    reg = hw.getNode("registers.pbf7.reps").write(reps)
    hw.dispatch()

    #Remove the reset of the mgt and of the playback
    print("Removing resets and starting playback")
    reg = hw.getNode("registers.control_1.mgt_reset").write(stop_flag)
    reg = hw.getNode("registers.pbf0.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf1.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf2.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf3.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf4.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf5.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf6.rst").write(stop_flag)
    reg = hw.getNode("registers.pbf7.rst").write(stop_flag)
    if (stop_flag == 0):
        reg = hw.getNode("registers.control_1.pbf_start").write(0x1)
        hw.dispatch()
        reg = hw.getNode("registers.control_1.pbf_start").write(0x0)



