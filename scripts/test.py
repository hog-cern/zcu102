import uhal
import numpy as np
import time

hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://10.0.0.31:50001", "file://zcu102.xml")
device_id = hw.id()
print hw
# Grab the device's URI
device_uri = hw.uri()
print device_uri

print hw.getNodes()

reg = hw.getNode("registers.control_1").write(0x0000)
hw.dispatch()

# Read the value back.
# NB: the reg variable below is a uHAL "ValWord", not just a simple integer
reg = hw.getNode("registers.status").read()
hw.dispatch()
print " status =", hex(reg)

reg = hw.getNode("registers.versions.firmware_version.version").read()
hw.dispatch()
print " ver =", hex(reg)

reg = hw.getNode("registers.versions.firmware_version.git_sha").read()
hw.dispatch()
print " sha =", hex(reg)

reg = hw.getNode("registers.versions.build_time_and_date.build_date").read()
hw.dispatch()
print " date =", hex(reg)

reg = hw.getNode("registers.versions.build_time_and_date.build_time").read()
hw.dispatch()
print " time =", hex(reg)

reg = hw.getNode("registers.versions.ipbus").read()
hw.dispatch()
print " ipbus =", hex(reg)

reg = hw.getNode("registers.versions.library.version").read()
hw.dispatch()
print " lib_ver =", hex(reg)

reg = hw.getNode("registers.versions.library.gitsha").read()
hw.dispatch()
print " lib sha =", hex(reg)

reg = hw.getNode("registers.versions.hog.version").read()
hw.dispatch()
print " hog =", hex(reg)

reg = hw.getNode("registers.versions.hog.gitsha").read()
hw.dispatch()
print " hog sha =", hex(reg)

reg = hw.getNode("registers.versions.xml_version.version").read()
hw.dispatch()
print " xml_ver =", hex(reg)

reg = hw.getNode("registers.versions.xml_version.git_sha").read()
hw.dispatch()
print " xml sha =", hex(reg)

reg = hw.getNode("registers.versions.top.version").read()
hw.dispatch()
print " top_ver =", hex(reg)

reg = hw.getNode("registers.versions.top.gitsha").read()
hw.dispatch()
print " top sha =", hex(reg)


reg = hw.getNode("registers.control_1").read()
hw.dispatch()
print hex(reg)


#Set the control registers for the playback functionalities
reg = hw.getNode("registers.pbf0.rst").write(0x1)
reg = hw.getNode("registers.pbf0.en").write(0x0)
reg = hw.getNode("registers.pbf0.loop").write(0x1)
reg = hw.getNode("registers.pbf0.load").write(0x0)
reg = hw.getNode("registers.pbf0.loadVal").write(0x0)
reg = hw.getNode("registers.pbf0.reps").write(0x1)

reg = hw.getNode("registers.pbf1.rst").write(0x1)
reg = hw.getNode("registers.pbf1.en").write(0x0)
reg = hw.getNode("registers.pbf1.loop").write(0x0)
reg = hw.getNode("registers.pbf1.load").write(0x0)
reg = hw.getNode("registers.pbf1.loadVal").write(0x0)
reg = hw.getNode("registers.pbf1.reps").write(0x1)

reg = hw.getNode("registers.pbf2.rst").write(0x1)
reg = hw.getNode("registers.pbf2.en").write(0x0)
reg = hw.getNode("registers.pbf2.loop").write(0x0)
reg = hw.getNode("registers.pbf2.load").write(0x0)
reg = hw.getNode("registers.pbf2.loadVal").write(0x0)
reg = hw.getNode("registers.pbf2.reps").write(0x1)

reg = hw.getNode("registers.pbf3.rst").write(0x1)
reg = hw.getNode("registers.pbf3.en").write(0x0)
reg = hw.getNode("registers.pbf3.loop").write(0x0)
reg = hw.getNode("registers.pbf3.load").write(0x0)
reg = hw.getNode("registers.pbf3.loadVal").write(0x0)
reg = hw.getNode("registers.pbf3.reps").write(0x1)

reg = hw.getNode("registers.pbf4.rst").write(0x1)
reg = hw.getNode("registers.pbf4.en").write(0x0)
reg = hw.getNode("registers.pbf4.loop").write(0x0)
reg = hw.getNode("registers.pbf4.load").write(0x0)
reg = hw.getNode("registers.pbf4.loadVal").write(0x0)
reg = hw.getNode("registers.pbf4.reps").write(0x1)

reg = hw.getNode("registers.pbf5.rst").write(0x1)
reg = hw.getNode("registers.pbf5.en").write(0x0)
reg = hw.getNode("registers.pbf5.loop").write(0x0)
reg = hw.getNode("registers.pbf5.load").write(0x0)
reg = hw.getNode("registers.pbf5.loadVal").write(0x0)
reg = hw.getNode("registers.pbf5.reps").write(0x1)

reg = hw.getNode("registers.pbf6.rst").write(0x1)
reg = hw.getNode("registers.pbf6.en").write(0x0)
reg = hw.getNode("registers.pbf6.loop").write(0x0)
reg = hw.getNode("registers.pbf6.load").write(0x0)
reg = hw.getNode("registers.pbf6.loadVal").write(0x0)
reg = hw.getNode("registers.pbf6.reps").write(0x1)

reg = hw.getNode("registers.pbf7.rst").write(0x1)
reg = hw.getNode("registers.pbf7.en").write(0x0)
reg = hw.getNode("registers.pbf7.loop").write(0x0)
reg = hw.getNode("registers.pbf7.load").write(0x0)
reg = hw.getNode("registers.pbf7.loadVal").write(0x0)
reg = hw.getNode("registers.pbf7.reps").write(0x1)

#Set the control registers for the readback functionalities
reg = hw.getNode("registers.rbf.rst").write(0x1)
reg = hw.getNode("registers.rbf.en").write(0x0)
reg = hw.getNode("registers.rbf.loop").write(0x0)
reg = hw.getNode("registers.rbf.load").write(0x0)
reg = hw.getNode("registers.rbf.loadVal").write(0x0)
reg = hw.getNode("registers.rbf.reps").write(0x3)
reg = hw.getNode("registers.rbf.discard_comma").write(0x1)

hw.dispatch()


N_data=65535
N_ctrl=N_data/8
# Fill list with random info
mgtData = []
mgtCtrl = []
mgtCommaData = []
mgtCommaCtrl = []
mgtNullData = []
mgtNullCtrl = []
for i in range(N_data):
   mgtData.append(3*i)
   mgtCommaData.append(0x00bc)
   mgtNullData.append(0)
   

for i in range(N_ctrl):
   mgtCtrl.append(0)
   mgtCommaCtrl.append(0x1111)

mgtData[0]=0xbc
mgtData[7]=0xbc
mgtCtrl[0]=0x10000001


# Write
#mem = hw.getNode("dprams_tx.data_dpram2").writeBlock(mgtData)
#mem2 = hw.getNode("dprams_tx.ctrl_dpram2").writeBlock(mgtCtrl)
#memTarget = [0, 1, 2, 3, 4, 5, 6, 7]
memTarget = [0]
for r in range(8):
   #Blank the memories
   mem = hw.getNode("dprams_tx.data_dpram"+str(r)).writeBlock(mgtNullData)
   mem = hw.getNode("dprams_tx.ctrl_dpram"+str(r)).writeBlock(mgtNullCtrl)
   if (r in memTarget):
      print"Random data in dprams_tx.data_dpram"+str(r)
      mem = hw.getNode("dprams_tx.data_dpram"+str(r)).writeBlock(mgtData)
      mem = hw.getNode("dprams_tx.ctrl_dpram"+str(r)).writeBlock(mgtCtrl)
   else:
      print"Comma  data in dprams_tx.data_dpram"+str(r)
      mem = hw.getNode("dprams_tx.data_dpram"+str(r)).writeBlock(mgtCommaData)
      mem = hw.getNode("dprams_tx.ctrl_dpram"+str(r)).writeBlock(mgtCommaCtrl)


#Reset the mgt and start the playback functionalities
reg = hw.getNode("registers.control_1.mgt_reset").write(1)
hw.dispatch()
reg = hw.getNode("registers.pbf0.rst").write(0x0)
reg = hw.getNode("registers.rbf.rst").write(0x0)
hw.dispatch()
reg = hw.getNode("registers.control_1.mgt_reset").write(0)
hw.dispatch()
reg = hw.getNode("registers.pbf0.en").write(0x1)
reg = hw.getNode("registers.rbf.en").write(0x1)
hw.dispatch()
reg = hw.getNode("registers.control_1.pbf_start").write(0x1)
hw.dispatch()
reg = hw.getNode("registers.control_1.pbf_start").write(0x0)
hw.dispatch()

time.sleep(1)

reg = hw.getNode("registers.pbf0.en").write(0x0)
reg = hw.getNode("registers.rbf.en").write(0x0)

# Read back values
print"dprams_tx.data_dpram"+str(memTarget[-1])
memDataTx = hw.getNode("dprams_tx.data_dpram"+str(memTarget[-1])).readBlock(N_data)
memCtrlTx = hw.getNode("dprams_tx.ctrl_dpram"+str(memTarget[-1])).readBlock(N_ctrl)

memDataRx = hw.getNode("dprams_rx.rx_data"+str(memTarget[-1])).readBlock(N_data)
memCtrlRx = hw.getNode("dprams_rx.rx_ctrl"+str(memTarget[-1])).readBlock(N_ctrl)


hw.dispatch()

# Use these values - either by array indexing, in a for loop, or print to screen
print " All TX data values:", memDataTx
print " All TX ctrl values:", memCtrlTx
print " All RX data values:", memDataRx
print " All RX ctrl values:", memCtrlRx
firstValue = memDataTx[0]
for x in memDataTx:
   pass
   # Do something with each value
