import sys # For sys.argv and sys.exit
import uhal

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "usage: list.py <xml file> <ip address>"
        sys.exit(1)

    xml_file = sys.argv[1];
    ip_add = sys.argv[2];



    hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://{0}:50001".format(ip_add), "file://{0}".format(xml_file))
    device_id = hw.id()
    print "Module name: ", hw

    device_uri = hw.uri()
    print "URI: ", device_uri
    
    nodes = hw.getNodes()
    print "***************"
    print "List of nodes: "
    for n in nodes:
       if hw.getNode(n).getMode() == uhal.BlockReadWriteMode.SINGLE and  hw.getNode(n).getMask() == 0xffffffff:
          reg = hw.getNode(n).read()
          hw.dispatch()
          print hw.getNode(n).getPath(), " = ", hex(reg)
       elif hw.getNode(n).getMode() == uhal.BlockReadWriteMode.INCREMENTAL:
          size = hw.getNode(n).getSize()
          mem = hw.getNode(n).readBlock(size)
          hw.dispatch()
          print "----------"
          print hw.getNode(n).getPath(), " = RAM, depth:", size , " locations"
          print [hex(x) for x in mem]
          print "----------"

#N=256
## Fill list with random info
#xx = []
#for i in range(N):
#   xx.append( i*7 )
#
## Write
#mem = hw.getNode("dpram_0").writeBlock(xx)
## Read back values
#mem = hw.getNode("dpram_0").readBlock(N)
#
#hw.dispatch()
#
