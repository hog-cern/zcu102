import uhal
import numpy as np

hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://10.0.0.31:50001", "file://zcu102.xml")
device_id = hw.id()
#print hw
# Grab the device's URI
device_uri = hw.uri()
#print device_uri

reg = hw.getNode("registers.control_1.mgt_reset").write(1)
reg = hw.getNode("registers.pbf0.rst").write(0x1)
reg = hw.getNode("registers.pbf0.en").write(0x0)
reg = hw.getNode("registers.pbf1.rst").write(0x1)
reg = hw.getNode("registers.pbf1.en").write(0x0)
reg = hw.getNode("registers.pbf2.rst").write(0x1)
reg = hw.getNode("registers.pbf2.en").write(0x0)
reg = hw.getNode("registers.pbf3.rst").write(0x1)
reg = hw.getNode("registers.pbf3.en").write(0x0)
reg = hw.getNode("registers.pbf4.rst").write(0x1)
reg = hw.getNode("registers.pbf4.en").write(0x0)
reg = hw.getNode("registers.pbf5.rst").write(0x1)
reg = hw.getNode("registers.pbf5.en").write(0x0)
reg = hw.getNode("registers.pbf6.rst").write(0x1)
reg = hw.getNode("registers.pbf6.en").write(0x0)
reg = hw.getNode("registers.pbf7.rst").write(0x1)
reg = hw.getNode("registers.pbf7.en").write(0x0)
hw.dispatch()

reg = hw.getNode("registers.control_1.mgt_reset").write(0)
hw.dispatch()

print("Reset MGTs and dpRAMs")
