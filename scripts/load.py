import sys # For sys.argv and sys.exit
import uhal

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print "usage: load.py <33bit file> <RAM number> <ip address>"
        sys.exit(1)

    txt_file = sys.argv[1]
    n_ram =sys.argv[2]
    ip_add = sys.argv[3]
    N=65535

    hw = uhal.getDevice( "zcu102" , "ipbusudp-2.0://{}:50001".format(ip_add), "file://../xml/zcu102.xml")

    f1 = open(txt_file, 'r') 
    lines = f1.readlines() 
    f1.close()
    mgtData = []
    mgtCtrl = []
    count = 0
    for line in lines:
       (d,c) = line.strip().split(" ");
       mgtData.append(int(d, 16))
       nibble = count % 8
       if nibble == 0:
          mgtCtrl.append(int(c))
       else:
          mgtCtrl[count//8] += int(c)*(0x10**nibble)
       count += 1
       if count > N:
          print "File has more than {} lines, skipping".format(N)
          break

    # Write
    mem = hw.getNode("dprams_tx.data_dpram{}".format(n_ram)).writeBlock(mgtData)
    mem2 = hw.getNode("dprams_tx.ctrl_dpram{}".format(n_ram)).writeBlock(mgtCtrl)
    hw.dispatch()
    print "{} lines written to memory".format(count)

    #Reset the mgt
    reg = hw.getNode("registers.control_1.mgt_reset").write(1)
    hw.dispatch()

    #Enable the relative playback functionalities (after reset)
    reg = hw.getNode("registers.pbf{}.rst".format(n_ram)).write(1)
    reg = hw.getNode("registers.pbf{}.en".format(n_ram)).write(1)
    hw.dispatch()
