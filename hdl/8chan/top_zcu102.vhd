--!@file top_zcu102.vhd
--!@brief Top-level design for DUNE ZCU102 mgt code development
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@author Francesco Gonnella, francesco.gonnella@cern.ch
--!@date 24/09/2020
--!@version 0.1 - 24/09/2020 -

--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.VComponents.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_reg_types.all;
use ipbus.all;

use work.ipbus_decode_zcu102.all;

use work.mgt_package.all;


--!@brief Top-level design for DUNE ZCU102 mgt code development
entity top_fmc0 is
  generic (
    -- Global Generic Variables
    GLOBAL_DATE : std_logic_vector(31 downto 0) := x"00000000";
    GLOBAL_TIME : std_logic_vector(31 downto 0) := x"00000000";
    GLOBAL_VER  : std_logic_vector(31 downto 0) := x"00000000";
    GLOBAL_SHA  : std_logic_vector(31 downto 0) := x"00000000";
    TOP_VER     : std_logic_vector(31 downto 0) := x"00000000";
    TOP_SHA     : std_logic_vector(31 downto 0) := x"00000000";
    CON_VER     : std_logic_vector(31 downto 0) := x"00000000";
    CON_SHA     : std_logic_vector(31 downto 0) := x"00000000";
    HOG_VER     : std_logic_vector(31 downto 0) := x"00000000";
    HOG_SHA     : std_logic_vector(31 downto 0) := x"00000000";
    --IPBus XML
    XML_SHA     : std_logic_vector(31 downto 0) := x"00000000";
    XML_VER     : std_logic_vector(31 downto 0) := x"00000000";
    -- Project Specific Lists (One for each .src file in your Top/myproj/list folder)
    FMC0_VER    : std_logic_vector(31 downto 0) := x"00000000";
    FMC0_SHA    : std_logic_vector(31 downto 0) := x"00000000";
    -- Submodule Specific variables (only if you have a submodule, one per submodule)
    IPBUS_SHA   : std_logic_vector(31 downto 0) := x"00000000";
    -- Project flavour
    FLAVOUR     : integer                       := 0
    );
  port(
    -- ipbus ports
    sysclk_p   : in  std_logic;                     -- 240 MHz??
    sysclk_n   : in  std_logic;
    eth_clk_p  : in  std_logic;                     -- 125MHz MGT clock
    eth_clk_n  : in  std_logic;
    eth_rx_p   : in  std_logic;                     -- Ethernet MGT input
    eth_rx_n   : in  std_logic;
    eth_tx_p   : out std_logic;                     -- Ethernet MGT output
    eth_tx_n   : out std_logic;
    sfp_enable : out std_logic;
    leds       : out std_logic_vector(7 downto 0);  -- status LEDs
    dip_sw     : in  std_logic_vector(3 downto 0);  -- switches


    -- GTH ports
    mgtrefclk0_x1y1_n, mgtrefclk0_x1y1_p : in  std_logic;
    ch0_gthrxn_in, ch0_gthrxp_in         : in  std_logic;
    ch0_gthtxn_out, ch0_gthtxp_out       : out std_logic;
    ch1_gthrxn_in, ch1_gthrxp_in         : in  std_logic;
    ch1_gthtxn_out, ch1_gthtxp_out       : out std_logic;
    ch2_gthrxn_in, ch2_gthrxp_in         : in  std_logic;
    ch2_gthtxn_out, ch2_gthtxp_out       : out std_logic;
    ch3_gthrxn_in, ch3_gthrxp_in         : in  std_logic;
    ch3_gthtxn_out, ch3_gthtxp_out       : out std_logic;
    ch4_gthrxn_in, ch4_gthrxp_in         : in  std_logic;
    ch4_gthtxn_out, ch4_gthtxp_out       : out std_logic;
    ch5_gthrxn_in, ch5_gthrxp_in         : in  std_logic;
    ch5_gthtxn_out, ch5_gthtxp_out       : out std_logic;
    ch6_gthrxn_in, ch6_gthrxp_in         : in  std_logic;
    ch6_gthtxn_out, ch6_gthtxp_out       : out std_logic;
    ch7_gthrxn_in, ch7_gthrxp_in         : in  std_logic;
    ch7_gthtxn_out, ch7_gthtxp_out       : out std_logic;

    -- Buttons and LEDs
    hb_gtwiz_reset_all_in      : in std_logic := '0';
    link_down_latched_reset_in : in std_logic := '0'
    );
end top_fmc0;

architecture rtl of top_fmc0 is
  -- Constants
  constant cN_CTRL       : positive := 10;  --!Number of control IPBus reg
  constant cN_STAT       : positive := 16;  --!Number of status IPBus reg
  constant cINIT_RST_LEN : positive := 6;  --!Clock cycles length of initial rst

  -- ipbus signals
  signal clk_ipb, clk_240           : std_logic := '0';
  signal rst_ipb, rst_aux, soft_rst : std_logic := '0';
  signal nuke                       : std_logic := '0';
  signal userled                    : std_logic := '0';
  signal mac_addr                   : std_logic_vector(47 downto 0);
  signal ip_addr                    : std_logic_vector(31 downto 0);

  signal ipb_in          : ipb_wbus;
  signal ipb_out         : ipb_rbus;
  signal ipb_to_slaves   : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipb_from_slaves : ipb_rbus_array(N_SLAVES-1 downto 0) := (others => IPB_RBUS_NULL);

  signal sIpbAsync : tSyncPort(cSYNC_IPBUS-1 downto 0);
  signal sIpbSync  : tSyncPort(cSYNC_IPBUS-1 downto 0);

  -- IPbus registers
  signal write_reg : ipb_reg_v(cN_STAT-1 downto 0) := (others => (others => '0'));
  signal read_reg  : ipb_reg_v(cN_CTRL-1 downto 0);

  signal status : std_logic_vector(31 downto 0);
  signal ctrl1  : std_logic_vector(31 downto 0) := (others => '0');
  type tPbf is array(cDPRAM_N-1 downto 0) of std_logic_vector(31 downto 0);
  signal sPbf   : tPbf;
  signal sRbf   : std_logic_vector(31 downto 0);

  signal sIpbResetMgt, sIpbResetMgtRising : std_logic := '0';

  signal sCtrlStart           : std_logic := '0';
  signal sCtrlSending         : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sCtrlRst, sCtrlEn    : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sCtrlLoop, sCtrlLoad : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sCtrlLoadVal         : tDpramTxAddrArray;
  signal sCtrlReps            : tRepCnt;

  signal sReadWriting : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sReadRst, sReadEn : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sReadLoop, sReadLoad : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sReadCDisc : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sReadLoadVal : tDpramRxAddrArray;
  signal sReadReps : tRepCnt;

  -- MGT signals
  signal sMgtTx       : tMgtArray(7 downto 0);
  signal sMgtUsrclkTx : std_logic;
  signal sMgtStatusTx : tMgtStatus;

  signal sMgtRx       : tMgtArray(7 downto 0);
  signal sMgtUsrclkRx : std_logic;
  signal sMgtStatusRx : tMgtStatus;

  signal sMgtIntSel : std_logic := '0';

  signal sInitRstEn   : std_logic            := '1';
  signal sInitRstCnt  : unsigned(3 downto 0) := (others => '0');
  signal sMgtResetAll : std_logic            := '1';

  -- MGT signals: the following are created just for ila
  signal sMgtRxIla                  : tMgtArray(7 downto 0);
  attribute mark_debug              : string;
  attribute mark_debug of sMgtRxIla : signal is "true";
  attribute mark_debug of sMgtTx    : signal is "true";

  -- Example project LEDs
  signal blinken_led, link_status_out, link_down_latched_out : std_logic;

  -- Ancillary signals
  signal sTxAddr     : tDpramTxAddrArray;
  signal sTxDpramOut : tMgtArray(cDPRAM_N-1 downto 0);

  signal sRxAddr     : tDpramRxAddrArray;
  signal sRxDpramWe  : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sRxDpramIn  : tMgtArray(cDPRAM_N-1 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Customisable values
  ------------------------------------------------------------------------------
  -- Ethernet connection
  sfp_enable <= '1';
  mac_addr   <= X"020ddba1151" & dip_sw;  -- Careful here, arbitrary addresses do not always work
  ip_addr    <= X"0a00001" & dip_sw;    -- 10.0.0.16+n

  -- LEDs
  leds(7 downto 2) <= blinken_led & link_status_out & link_down_latched_out & "00" & userled;

  --MGT
  sMgtIntSel <= '0';

  ------------------------------------------------------------------------------
  -- REGISTER CONNECTIONS
  ------------------------------------------------------------------------------
  write_reg(0)  <= status;
  write_reg(1)  <= GLOBAL_VER;
  write_reg(2)  <= GLOBAL_SHA;
  write_reg(3)  <= GLOBAL_DATE;
  write_reg(4)  <= GLOBAL_TIME;
  write_reg(5)  <= XML_VER;
  write_reg(6)  <= XML_SHA;
  write_reg(7)  <= HOG_VER;
  write_reg(8)  <= HOG_SHA;
  write_reg(9)  <= CON_VER;
  write_reg(10) <= CON_SHA;
  write_reg(11) <= TOP_VER;
  write_reg(12) <= TOP_SHA;
  write_reg(13) <= FMC0_VER;
  write_reg(14) <= FMC0_SHA;
  write_reg(15) <= IPBUS_SHA;

  ctrl1   <= read_reg(0);
  sPbf(0) <= read_reg(1);
  sPbf(1) <= read_reg(2);
  sPbf(2) <= read_reg(3);
  sPbf(3) <= read_reg(4);
  sPbf(4) <= read_reg(5);
  sPbf(5) <= read_reg(6);
  sPbf(6) <= read_reg(7);
  sPbf(7) <= read_reg(8);
  sRbf    <= read_reg(9);

  sIpbResetMgt <= ctrl1(0);
  sCtrlStart   <= ctrl1(31);
  status       <=   sCtrlSending
                  & x"0000"
                  & "0"
                  & sIpbSync(0)
                  & sIpbSync(1)
                  & sIpbSync(2)
                  & sIpbSync(3)
                  & sIpbSync(4)
                  & sIpbSync(5)
                  & (not sMgtResetAll);

  --@!brief Playback functionalities assignment
  pbf_gen : for i in 0 to cDPRAM_N-1 generate
    sCtrlRst(i)     <= sPbf(i)(31);
    sCtrlEn(i)      <= sPbf(i)(30);
    sCtrlLoop(i)    <= sPbf(i)(29);
    sCtrlLoad(i)    <= sPbf(i)(28);
    sCtrlLoadVal(i)(9 downto 0) <= sPbf(i)(25 downto 16);
    sCtrlLoadVal(i)(15 downto 10) <= (others => '0');
    sCtrlReps(i)    <= sPbf(i)(15 downto 0);

    sReadRst(i)     <= sRbf(31);
    sReadEn(i)      <= sRbf(30);
    sReadLoop(i)    <= sRbf(29);
    sReadLoad(i)    <= sRbf(28);
    sReadCDisc(i)   <= sRbf(27);
    sReadLoadVal(i) <= sRbf(25 downto 16);
    sReadReps(i)    <= sRbf(15 downto 0);
  end generate pbf_gen;

  ------------------------------------------------------------------------------
  -- IPBUS CODE
  ------------------------------------------------------------------------------
  --!@brief Infrastructure
  infra : entity ipbus.zcu102_basex_infra
    generic map(
      CLK_AUX_FREQ => 240.0
      )
    port map(
      sysclk_p  => sysclk_p,
      sysclk_n  => sysclk_n,
      eth_clk_p => eth_clk_p,
      eth_clk_n => eth_clk_n,
      eth_tx_p  => eth_tx_p,
      eth_tx_n  => eth_tx_n,
      eth_rx_p  => eth_rx_p,
      eth_rx_n  => eth_rx_n,
      sfp_los   => '0',
      clk_ipb_o => clk_ipb,
      rst_ipb_o => rst_ipb,
      clk_aux_o => clk_240,
      rst_aux_o => rst_aux,
      nuke      => nuke,
      soft_rst  => soft_rst,
      leds      => leds(1 downto 0),
      mac_addr  => mac_addr,
      ip_addr   => ip_addr,
      ipb_in    => ipb_out,
      ipb_out   => ipb_in
      );

  --!@brief IPbus fabric
  IPBUS_FABRIC_TOP : entity ipbus.ipbus_fabric_sel
    generic map (
      NSLV      => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map (
      sel             => ipbus_sel_zcu102(ipb_in.ipb_addr),
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
      );

  --!@brief IPbus register bank
  IPBUS_REGISTERS : entity ipbus.ipbus_ctrlreg_v
    generic map (
      N_CTRL => cN_CTRL,
      N_STAT => cN_STAT)
    port map (
      clk       => clk_ipb,
      reset     => rst_ipb,
      ipbus_in  => ipb_to_slaves(N_SLV_REGISTERS),
      ipbus_out => ipb_from_slaves(N_SLV_REGISTERS),
      d         => write_reg,
      q         => read_reg,
      stb       => open
      );

  --!@brief IPbus TX dpram bank
  TX_DPRAMS : entity work.ndpram_datactrl
    port map(
      iIPB_CLK       => clk_ipb,
      iIPB_RST       => rst_ipb,
      iIPB           => ipb_to_slaves(N_SLV_DPRAMS_TX),
      oIPB           => ipb_from_slaves(N_SLV_DPRAMS_TX),
      iRAM_CLK       => sMgtUsrclkTx,
      iRAM_ADDR      => sTxAddr,
      oRAM_DATA_CTRL => sTxDpramOut
      );

  --!@brief IPbus RX dpram bank
  RX_DPRAMS : entity work.ndpram_rx
    port map(
      iIPB_CLK       => clk_ipb,
      iIPB_RST       => rst_ipb,
      iIPB           => ipb_to_slaves(N_SLV_DPRAMS_RX),
      oIPB           => ipb_from_slaves(N_SLV_DPRAMS_RX),
      iRAM_CLK       => sMgtUsrclkRx,
      iRAM_WE        => sRxDpramWe,
      iRAM_ADDR      => sRxAddr,
      iRAM_DATA_CTRL => sRxDpramIn
      );

  --!@brief Synch to ipbus clock
  IPBUS_SYNC : entity work.sync_wrapper
    generic map (
      pWIDTH  => cSYNC_IPBUS,
      pSTAGES => 2)
    port map (
      iCLK   => clk_ipb,
      iASYNC => sIpbAsync,
      oSYNC  => sIpbSync);
  sIpbAsync(0) <= sMgtStatusRx.active;
  sIpbAsync(1) <= sMgtStatusRx.reset;
  sIpbAsync(2) <= sMgtStatusRx.resetDone;
  sIpbAsync(3) <= sMgtStatusTx.active;
  sIpbAsync(4) <= sMgtStatusTx.reset;
  sIpbAsync(5) <= sMgtStatusTx.resetDone;

  ------------------------------------------------------------------------------
  -- GTH wrapper
  ------------------------------------------------------------------------------
  --!@brief Wraps the GTH example design for 8-channel FMC1.
  --!@test There are more CTRL ports that are not connected
  FMC0 : entity work.zcu102_mgt_wrapper
    port map (
      --Differential reference clock inputs
      mgtrefclk0_x1y1_n             => mgtrefclk0_x1y1_n,
      mgtrefclk0_x1y1_p             => mgtrefclk0_x1y1_p,
      --Serial data ports for transceiver channel 0
      ch0_gthrxn_in                 => ch0_gthrxn_in,
      ch0_gthrxp_in                 => ch0_gthrxp_in,
      ch0_gthtxn_out                => ch0_gthtxn_out,
      ch0_gthtxp_out                => ch0_gthtxp_out,
      --Serial data ports for transceiver channel 1
      ch1_gthrxn_in                 => ch1_gthrxn_in,
      ch1_gthrxp_in                 => ch1_gthrxp_in,
      ch1_gthtxn_out                => ch1_gthtxn_out,
      ch1_gthtxp_out                => ch1_gthtxp_out,
      --Serial data ports for transceiver channel 2
      ch2_gthrxn_in                 => ch2_gthrxn_in,
      ch2_gthrxp_in                 => ch2_gthrxp_in,
      ch2_gthtxn_out                => ch2_gthtxn_out,
      ch2_gthtxp_out                => ch2_gthtxp_out,
      --Serial data ports for transceiver channel 3
      ch3_gthrxn_in                 => ch3_gthrxn_in,
      ch3_gthrxp_in                 => ch3_gthrxp_in,
      ch3_gthtxn_out                => ch3_gthtxn_out,
      ch3_gthtxp_out                => ch3_gthtxp_out,
      --Serial data ports for transceiver channel 4
      ch4_gthrxn_in                 => ch4_gthrxn_in,
      ch4_gthrxp_in                 => ch4_gthrxp_in,
      ch4_gthtxn_out                => ch4_gthtxn_out,
      ch4_gthtxp_out                => ch4_gthtxp_out,
      --Serial data ports for transceiver channel 5
      ch5_gthrxn_in                 => ch5_gthrxn_in,
      ch5_gthrxp_in                 => ch5_gthrxp_in,
      ch5_gthtxn_out                => ch5_gthtxn_out,
      ch5_gthtxp_out                => ch5_gthtxp_out,
      --Serial data ports for transceiver channel 6
      ch6_gthrxn_in                 => ch6_gthrxn_in,
      ch6_gthrxp_in                 => ch6_gthrxp_in,
      ch6_gthtxn_out                => ch6_gthtxn_out,
      ch6_gthtxp_out                => ch6_gthtxp_out,
      --Serial data ports for transceiver channel 7
      ch7_gthrxn_in                 => ch7_gthrxn_in,
      ch7_gthrxp_in                 => ch7_gthrxp_in,
      ch7_gthtxn_out                => ch7_gthtxn_out,
      ch7_gthtxp_out                => ch7_gthtxp_out,
      --User MGT connection TX
      iMGT_TX                       => sMgtTx,
      oMGT_USRCLK_TX                => sMgtUsrclkTx,
      oMGT_STATUS_TX                => sMgtStatusTx,
      --User MGT connection RX
      oMGT_RX                       => sMgtRx,
      oMGT_USRCLK_RX                => sMgtUsrclkRx,
      oMGT_STATUS_RX                => sMgtStatusRx,
      --User CTRL signals
      iMGT_INT_SEL                  => sMgtIntSel,
      --User-provided ports for reset helper block(s)
      hb_gtwiz_reset_clk_freerun_in => clk_240,
      hb_gtwiz_reset_all_in         => sMgtResetAll,
      --PRBS-based link status ports
      link_down_latched_reset_in    => link_down_latched_reset_in,
      link_status_out               => link_status_out,
      link_down_latched_out         => link_down_latched_out
      );

  ------------------------------------------------------------------------------
  -- Ancillary
  ------------------------------------------------------------------------------
  --!@brief Blinking LED
  BLINKING_LED : entity work.clock_div port map(
    clk => clk_240,
    d28 => blinken_led
    );

  --!@brief Reset the MGTs at the rising edge of a control signal (after synch)
  SYNC_EDGE_MGT_RESET : entity work.sync_edge
    generic map (
      pSTAGES => 3
      )
    port map (
        iCLK    => clk_240,
        iRST    => '0',
        iD      => sIpbResetMgt,
        oQ      => open,
        oEDGE_R => sIpbResetMgtRising,
        oEDGE_F => open
        );

  --!@brief Initial reset for the MGTs
  MGT_INITIAL_RESET : process (clk_240)
  begin
    if rising_edge(clk_240) then
      if (sInitRstEn = '1') then
        sInitRstCnt <= sInitRstCnt+1;
      end if;
      if (sInitRstCnt < cINIT_RST_LEN) then
        sInitRstEn   <= '1';
        sMgtResetAll <= '1';
      else
        sInitRstEn   <= '0';
        sMgtResetAll <= hb_gtwiz_reset_all_in or sIpbResetMgtRising;
      end if;
    end if;
  end process;

  --!@brief Playback functionalities for 8 channels
  --!@todo Add the tx reset and enable from the MGTs
  PLAYBACK_LOGIC : entity work.playback_ctrl
    port map (
      iCLK          => sMgtUsrclkTx,
      iSTART        => sCtrlStart,
      oSENDING      => sCtrlSending,
      iCFG_RST      => sCtrlRst,
      iCFG_EN       => sCtrlEn,
      iCFG_LOOP     => sCtrlLoop,
      iCFG_LOAD     => sCtrlLoad,
      iCFG_LOAD_VAL => sCtrlLoadVal,
      iCFG_REPS     => sCtrlReps,
      iRAM_DATA     => sTxDpramOut,
      oRAM_ADDR     => sTxAddr,
      oMGT_DATA     => sMgtTx
      );

  --!@brief Readback functionalities for 8 channels
  --!@todo Add the tx reset and enable from the MGTs
  READBACK_LOGIC : entity work.rx_ctrl
    port map (
      iCLK          => sMgtUsrclkRx,
      oWRITING      => sReadWriting,
      iCFG_RST      => sReadRst,
      iCFG_EN       => sReadEn,
      iCFG_CDISC    => sReadCDisc,
      iCFG_LOOP     => sReadLoop,
      iCFG_LOAD     => sReadLoad,
      iCFG_LOAD_VAL => sReadLoadVal,
      iCFG_REPS     => sReadReps,
      iMGT_DATA     => sMgtRx,
      oRAM_WE       => sRxDpramWe,
      oRAM_ADDR     => sRxAddr,
      oRAM_DATA     => sRxDpramIn
    );


  -- ILA connections
  sMgtRxIla <= sMgtRx;

end rtl;
