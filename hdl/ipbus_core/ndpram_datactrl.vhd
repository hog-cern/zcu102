--!@file ndpram_datactrl.vhd
--!@brief Instantiate a variable number of data and ctrl dprams
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 24/09/2020
--!@version 0.1 - 24/09/2020 -
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.VComponents.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_reg_types.all;
use ipbus.all;

use work.ipbus_decode_ndpram_datactrl.all;

use work.mgt_package.all;

--!@brief Instantiate a variable number of data and ctrl dprams
entity ndpram_datactrl is
  port(
    --IPbus ports
    iIPB_CLK       : in  std_logic;
    iIPB_RST       : in  std_logic;
    iIPB           : in  ipb_wbus;
    oIPB           : out ipb_rbus;
    --DPRAMs ports
    iRAM_CLK       : in  std_logic;
    iRAM_ADDR      : in  tDpramTxAddrArray;
    oRAM_DATA_CTRL : out tMgtArray(cDPRAM_N-1 downto 0)
    );
end ndpram_datactrl;


architecture std of ndpram_datactrl is
  constant cCTRL_ADDR_DIV : natural range 0 to 31 := 3;

  signal sIpbToSlaves   : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal sIpbFromSlaves : ipb_rbus_array(N_SLAVES-1 downto 0)
    := (others => IPB_RBUS_NULL);

  type tCtrlQ is array(natural range <>)
    of std_logic_vector(cDPRAM_DATA_WIDTH-1 downto 0);
  signal sCtrlQ : tCtrlQ(cDPRAM_N-1 downto 0);

  type tCtrlSel is array(natural range <>) of natural range 0 to 2**cCTRL_ADDR_DIV-1;
  signal sCtrlSel : tCtrlSel(cDPRAM_N-1 downto 0);

begin
  --!@brief ipbus fabric to decode the addresses
  IPBUS_FABRIC : entity ipbus.ipbus_fabric_sel
    generic map (
      NSLV      => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH
      )
    port map (
      sel             => ipbus_sel_ndpram_datactrl(iIPB.ipb_addr),
      ipb_in          => iIPB,
      ipb_out         => oIPB,
      ipb_to_slaves   => sIpbToSlaves,
      ipb_from_slaves => sIpbFromSlaves
      );

  --!@brief Connect a variable number of data and ctrl DPRAMs
  DPRAMS_GENERATE : for i in 0 to cDPRAM_N-1 generate
    --!@brief Data dual-port RAM
    DATA_DPRAM : entity ipbus.ipbus_dpram
      generic map (
        ADDR_WIDTH => cDPRAM_TX_ADDR_WIDTH,
        DATA_WIDTH => cDPRAM_DATA_WIDTH
        )
      port map (
        clk     => iIPB_CLK,
        rst     => iIPB_RST,
        ipb_in  => sIpbToSlaves(N_SLV_DATA_DPRAM0+i),
        ipb_out => sIpbFromSlaves(N_SLV_DATA_DPRAM0+i),
        rclk    => iRAM_CLK,
        we      => '0',
        d       => (others => '0'),
        q       => oRAM_DATA_CTRL(i).data,
        addr    => iRAM_ADDR(i)
        );

    --!@brief Multiplex the ctrl to the output port
    --!@details Each 32-bit word of the RAM has four 4-bit ctrl words that have
    -- to be multiplexed
    --!@param[in] iRAM_ADDR Address to be read
    --!@return oRAM_DATA_CTRL.ctrl Control bits for the MGT
    ctrl_mux_proc : process(iRAM_CLK)
    begin
      if (rising_edge(iRAM_CLK)) then
        sCtrlSel(i) <= to_integer(unsigned(
                        iRAM_ADDR(i)(cCTRL_ADDR_DIV-1 downto 0)));
      end if;  --rising_edge(iRAM_CLK)
    end process ctrl_mux_proc;
    oRAM_DATA_CTRL(i).ctrl <= "0000"
                              & sCtrlQ(i)(3+4*sCtrlSel(i) downto 4*sCtrlSel(i));

    --!@brief Ctrl dual-port RAM
    CTRL_DPRAM : entity ipbus.ipbus_dpram
      generic map (
        ADDR_WIDTH => cDPRAM_TX_ADDR_WIDTH-cCTRL_ADDR_DIV,
        DATA_WIDTH => cDPRAM_DATA_WIDTH
        )
      port map (
        clk     => iIPB_CLK,
        rst     => iIPB_RST,
        ipb_in  => sIpbToSlaves(N_SLV_CTRL_DPRAM0+i),
        ipb_out => sIpbFromSlaves(N_SLV_CTRL_DPRAM0+i),
        rclk    => iRAM_CLK,
        we      => '0',
        d       => (others => '0'),
        q       => sCtrlQ(i),
        addr    => iRAM_ADDR(i)(iRAM_ADDR(i)'left downto cCTRL_ADDR_DIV)
        );

  end generate DPRAMS_GENERATE;


end architecture;
