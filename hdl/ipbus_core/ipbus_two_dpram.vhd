--!@file ipbus_two_dpram.vhd
--!@brief Embed two dpram to generate wib data and ctrl
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 21/09/2020
--!@version 0.1 - 21/09/2020 -
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.VComponents.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_reg_types.all;
use ipbus.all;

use work.ipbus_decode_ipbus_two_dpram.all;

use work.mgt_package.all;

--!@brief Embed two dpram to generate wib data and ctrl
entity ipbus_two_dpram is
  generic (
    pDATA_WIDTH : natural range 0 to 127 := 32;
    pADDR_WIDTH : natural range 0 to 127 := 8
    );
  port(
    --IPbus ports
    iIPB_CLK       : in  std_logic;
    iIPB_RST       : in  std_logic;
    iIPB           : in  ipb_wbus;
    oIPB           : out ipb_rbus;
    --DPRAMs ports
    iRAM_CLK       : in  std_logic;
    iRAM_ADDR      : in  std_logic_vector(pADDR_WIDTH-1 downto 0);
    oRAM_DATA_CTRL : out tMgtCh
    );
end ipbus_two_dpram;


architecture std of ipbus_two_dpram is
  signal sIpbToSlaves   : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal sIpbFromSlaves : ipb_rbus_array(N_SLAVES-1 downto 0)
    := (others => IPB_RBUS_NULL);

  signal sCtrlQ : std_logic_vector(pDATA_WIDTH-1 downto 0);
  signal sCtrlSel : natural range 0 to 3 := 0;

begin
  --!@brief ipbus fabric to decode the addresses
  IPBUS_FABRIC : entity ipbus.ipbus_fabric_sel
    generic map (
      NSLV      => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH
      )
    port map (
      sel             => ipbus_sel_ipbus_two_dpram(iIPB.ipb_addr),
      ipb_in          => iIPB,
      ipb_out         => oIPB,
      ipb_to_slaves   => sIpbToSlaves,
      ipb_from_slaves => sIpbFromSlaves
      );

  --!@brief Data dual-port RAM
  DATA_DPRAM : entity ipbus.ipbus_dpram
    generic map (
      ADDR_WIDTH => pADDR_WIDTH,
      DATA_WIDTH => pDATA_WIDTH
      )
    port map (
      clk     => iIPB_CLK,
      rst     => iIPB_RST,
      ipb_in  => sIpbToSlaves(N_SLV_DATA_DPRAM),
      ipb_out => sIpbFromSlaves(N_SLV_DATA_DPRAM),
      rclk    => iRAM_CLK,
      we      => '0',
      d       => (others => '0'),
      q       => oRAM_DATA_CTRL.data,
      addr    => iRAM_ADDR
      );

  --!@brief Multiplex the ctrl to the output port
  --!@details Each 32-bit word of the RAM has four 4-bit ctrl words that have
  -- to be multiplexed
  --!@param[in] iRAM_ADDR Address to be read
  --!@return oRAM_DATA_CTRL.ctrl Control bits for the MGT
  ctrl_mux_proc : process(iRAM_CLK)
  begin
    if (rising_edge(iRAM_CLK)) then
      sCtrlSel            <= to_integer(unsigned(iRAM_ADDR(1 downto 0)));
    end if;  --rising_edge(iRAM_CLK)
  end process ctrl_mux_proc;
  oRAM_DATA_CTRL.ctrl <= "0000" & sCtrlQ(3+4*sCtrlSel downto 4*sCtrlSel);

  --!@brief Ctrl dual-port RAM
  CTRL_DPRAM : entity ipbus.ipbus_dpram
    generic map (
      ADDR_WIDTH => pADDR_WIDTH-2,
      DATA_WIDTH => pDATA_WIDTH
      )
    port map (
      clk     => iIPB_CLK,
      rst     => iIPB_RST,
      ipb_in  => sIpbToSlaves(N_SLV_CTRL_DPRAM),
      ipb_out => sIpbFromSlaves(N_SLV_CTRL_DPRAM),
      rclk    => iRAM_CLK,
      we      => '0',
      d       => (others => '0'),
      q       => sCtrlQ,
      addr    => iRAM_ADDR(iRAM_ADDR'left downto 2)
      );



end architecture;
