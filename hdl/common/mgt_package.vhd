--!@file mgt_package.vhd
--!@brief Constants, components declarations and functions
--!@author Richard Staley, richard.staley@cern.ch
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 17/09/2020
--!@version 0.1 - 17/09/2020 -

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--!@brief Constants, components declarations and functions
package mgt_package is
  constant cCOMMA_DATA          : std_logic_vector(31 downto 0) := x"000000BC";
  constant cCOMMA_CTRL          : std_logic_vector(7 downto 0)  := x"01";
  constant cDPRAM_N             : natural range 0 to 31         := 8;
  constant cDPRAM_DATA_WIDTH    : natural range 0 to 127        := 32;
  constant cDPRAM_TX_ADDR_WIDTH : natural range 0 to 127        := 16;
  constant cDPRAM_RX_ADDR_WIDTH : natural range 0 to 127        := 10;
  constant cPF_REP_WIDTH        : natural range 0 to 31         := 16;
  constant cSYNC_IPBUS          : natural                       := 6;

  --!Interface for a single GTH channel data and ctrl signals
  type tMgtCh is record
    data : std_logic_vector(31 downto 0);
    ctrl : std_logic_vector(7 downto 0);
  end record tMgtCh;

  constant cCOMMA_CH : tMgtCh := (cCOMMA_DATA, cCOMMA_CTRL);

  --!Status signals for MGT
  type tMgtStatus is record
    active      : std_logic;
    reset       : std_logic;
    resetDone   : std_logic;
    byteAligned : std_logic_vector(7 downto 0);
  end record tMgtStatus;

  --!Interface for a variable number of MGT channels
  type tMgtArray is array(natural range <>) of tMgtCh;

  --!Array of TX DPRAM addresses
  type tDpramTxAddrArray is array(cDPRAM_N-1 downto 0) of
    std_logic_vector(cDPRAM_TX_ADDR_WIDTH-1 downto 0);

  --!Array of RX DPRAM addresses
  type tDpramRxAddrArray is array(cDPRAM_N-1 downto 0) of
    std_logic_vector(cDPRAM_RX_ADDR_WIDTH-1 downto 0);

  --!Array of Reps counters
  type tRepCnt is array(cDPRAM_N-1 downto 0) of
    std_logic_vector(cPF_REP_WIDTH-1 downto 0);

  type tSyncPort is array(natural range <>) of
    std_logic;

end mgt_package;
