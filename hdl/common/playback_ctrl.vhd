--!@file playback_ctrl.vhd
--!@brief Playback functionalities for the dual-port RAMs
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 07/10/2020
--!@version 0.1 - 07/10/2020 -

--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.mgt_package.all;

--!@brief Playback functionalities for the dual-port RAMs
entity playback_ctrl is
  port(
    iCLK          : in  std_logic;
    iSTART        : in  std_logic;
    oSENDING      : out std_logic_vector(cDPRAM_N-1 downto 0);
    --
    iCFG_RST      : in  std_logic_vector(cDPRAM_N-1 downto 0);
    iCFG_EN       : in  std_logic_vector(cDPRAM_N-1 downto 0);
    iCFG_LOOP     : in  std_logic_vector(cDPRAM_N-1 downto 0);
    iCFG_LOAD     : in  std_logic_vector(cDPRAM_N-1 downto 0);
    iCFG_LOAD_VAL : in  tDpramTxAddrArray;
    iCFG_REPS     : in  tRepCnt;
    --
    iRAM_DATA     : in  tMgtArray(cDPRAM_N-1 downto 0);
    oRAM_ADDR     : out tDpramTxAddrArray;
    --
    oMGT_DATA     : out tMgtArray(cDPRAM_N-1 downto 0)
    );
end playback_ctrl;

architecture std of playback_ctrl is
  signal sStartRis : std_logic;
  signal sChRst    : std_logic_vector(cDPRAM_N-1 downto 0);

  signal sAddrCnt : tDpramTxAddrArray;
  signal sAddrMax : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sMaxReps : std_logic_vector(cDPRAM_N-1 downto 0);
  signal sRepsCnt : tRepCnt;

begin
  oRAM_ADDR <= sAddrCnt;

  --!@brief Detect the rising edge of the start signal
  SYNC_GENERATE_START_EDGE : entity work.sync_edge
    generic map (
      pSTAGES => 3
      )
    port map (
      iCLK    => iCLK,
      iRST    => '0',
      iD      => iSTART,
      oQ      => open,
      oEDGE_R => sStartRis,
      oEDGE_F => open
      );


  --!@brief Generate a control electronics for each dpram
  CTRL_GEN : for i in 0 to cDPRAM_N-1 generate
    sChRst(i)   <= iCFG_RST(i) or sStartRis;
    oSENDING(i) <= iCFG_EN(i) and not sChRst(i);

    --!@brief Address counter that loops forever
    ADDR_COUNTER : entity work.counter
      generic map (
        pOVERLAP  => "Y",
        pBUSWIDTH => cDPRAM_TX_ADDR_WIDTH
        ) port map (
          iCLK   => iCLK,
          iEN    => iCFG_EN(i),
          iRST   => sChRst(i) or sMaxReps(i),
          iLOAD  => iCFG_LOAD(i),
          iDATA  => iCFG_LOAD_VAL(i),
          oCOUNT => sAddrCnt(i),
          oMAX   => sAddrMax(i),
          oCARRY => open
          );

    --!@brief Count the repetitions number
    REPS_COUNTER : entity work.counter
      generic map (
        pOVERLAP  => "Y",
        pBUSWIDTH => cPF_REP_WIDTH
        ) port map (
          iCLK   => iCLK,
          iEN    => sAddrMax(i) and not sMaxReps(i),
          iRST   => sChRst(i),
          iLOAD  => '0',
          iDATA  => (others => '0'),
          oCOUNT => sRepsCnt(i),
          oMAX   => open,
          oCARRY => open
          );

    --!@brief Mux to connect the proper data or the comma character
    COMMA_PROC : process(iCLK)
    begin
      if (rising_edge(iCLK)) then
        if(sChRst(i) = '1') then
          oMGT_DATA(i) <= cCOMMA_CH;
          sMaxReps(i)  <= '0';
        else
          if (iCFG_EN(i) = '1' and ((sRepsCnt(i) < iCFG_REPS(i)) or iCFG_LOOP(i) = '1')) then
            oMGT_DATA(i) <= iRAM_DATA(i);
            sMaxReps(i) <= '0';
          else
            oMGT_DATA(i) <= cCOMMA_CH;
            sMaxReps(i)  <= '1';
          end if;
        end if;
      end if;
    end process COMMA_PROC;

  end generate CTRL_GEN;

end architecture;
