--!@file sync_wrapper.vhd
--!@brief Embeds the sync_stage elements for a specific clock
--!@author Mattia Barbanera, mattia.barbanera@cern.ch
--!@date 22/10/2020
--!@version 0.1 - 22/10/2020 -

--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.mgt_package.all;

--!@brief Embeds the sync_stage elements for a specific clock
entity sync_wrapper is
  generic(
    pWIDTH  : natural := 8;
    pSTAGES : natural := 2
    );
  port (
    iCLK   : in  std_logic;
    iASYNC : in  tSyncPort(pWIDTH-1 downto 0);
    oSYNC  : out tSyncPort(pWIDTH-1 downto 0)
    );
end entity sync_wrapper;

architecture std of sync_wrapper is

begin  -- architecture std
  --!@brief Connect as many sync_stage as needed
  SYNC_GENERATE : for kk in 0 to pWIDTH-1 generate
    sync_el : entity work.sync_stage
      generic map(
        pSTAGES => pSTAGES
        )
      port map(
        iCLK => iCLK,
        iRST => '0',
        iD   => iASYNC(kk),
        oQ   => oSYNC(kk)
        );
  end generate SYNC_GENERATE;
end architecture std;
