-- Data-source for MGT transmitters sending WIB-like test data
--
-- Named wbone = wishbone = custom ibpus slave module (not to be confused with code from ipbus repository)
--
-- Richard Staley, April 2019


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.mgt.all;


entity wbone_data_source is 
	generic(
		DPRAM_ADDR_WIDTH: natural := 8; -- the size of this block which includes bath data and ctrl ram
		FRAME_SIZE: natural := 120 -- 120 32 bit words
    );
	port(
		ipbus_clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
		data_clk: in std_logic;
        data_out: out mgt_data
	);
	
end wbone_data_source;

architecture rtl of wbone_data_source is

    signal pointer_addr: std_logic_vector(DPRAM_ADDR_WIDTH-2 downto 0);
    signal ram_index:            unsigned(DPRAM_ADDR_WIDTH-2 downto 0);

    signal ipbw: ipb_wbus_array(1 downto 0);
    signal ipbr: ipb_rbus_array(1 downto 0);

	signal null_data: std_logic_vector(31 downto 0) := (others => '0');
	signal null_ctrl: std_logic_vector(3 downto 0) := (others => '0');
	signal web: std_logic;


 	signal source_data: std_logic_vector(31 downto 0) := (others => '0');
    signal source_ctrl: std_logic_vector(3 downto 0) := (others => '0');
    signal ram_data  : mgt_data;
    
    constant RAM_SIZE : natural := 2** (DPRAM_ADDR_WIDTH - 1 ); 

begin

ram_pointer: entity work.tx_ram_pointer
	generic map(
		DPRAM_ADDR_WIDTH => DPRAM_ADDR_WIDTH-1
	)
	port map(
		data_clk   => data_clk,
		ram_index  => ram_index
	);


   web <= '0';

    pointer_addr <= std_logic_vector(ram_index);


block_decode: entity work.wbone_fabric_branch
    generic map(
    NSLV => 2,
    DECODE_BASE => DPRAM_ADDR_WIDTH - 1
    )
	port map(
        ipb_in => ipbus_in,
        ipb_out => ipbus_out,
        ipb_to_slaves => ipbw,
        ipb_from_slaves => ipbr
    );

dssram: entity work.wbone_dpram_data_init
	generic map(
		ADDR_WIDTH => DPRAM_ADDR_WIDTH-1
	)
	port map(
		clk => ipbus_clk,
		rst => reset,
		ipb_in => ipbw(0),
		ipb_out => ipbr(0),
		rclk => data_clk,
		we => web,
		d => null_data,
		q => source_data, 
		addr => pointer_addr
	);

ctrlram: entity work.wbone_dpram_ctrl_init
	generic map(
		ADDR_WIDTH => DPRAM_ADDR_WIDTH-1
	)
	port map(
		clk => ipbus_clk,
		rst => reset,
		ipb_in => ipbw(1),
		ipb_out => ipbr(1),
		rclk => data_clk,
		we => web,
		d => null_ctrl,
		q => source_ctrl, 
		addr => pointer_addr
	);

    ram_data.data <= source_data;
    ram_data.ctrl <= source_ctrl;

    data_out <= ram_data;


end rtl;
