-- wbone_dpram_ctrl_init.vhd
-- to match the WIB test data
--
-- R. Staley, Sept 2017. Memory 4-bits wide for ctrl bits to run in parallel 
--                       with playback data to convert byte into K-comma 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.ipbus.all;

entity wbone_dpram_ctrl_init is
	generic(
		ADDR_WIDTH: natural
	);
	port(
		clk: in std_logic;
		rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		rclk: in std_logic;
		we: in std_logic := '0';
		d: in std_logic_vector(3 downto 0) := (others => '0');
		q: out std_logic_vector(3 downto 0);
		addr: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
	
end wbone_dpram_ctrl_init;

architecture rtl of wbone_dpram_ctrl_init is

	type ram_array is array(0 to 2 ** ADDR_WIDTH - 1 ) of std_logic_vector(3 downto 0); -- to allow initialisation from address 0
	
	shared variable ram: ram_array :=(
   "0001", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", 
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000",
   "0000", "0000", "0000", "0000", "0000", "0000", "0000", "0001", "0001", "0001",
    others => "0001" ); -- WIB IDLE
	
	signal sel, rsel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
	signal ack: std_logic;
   signal rdata, wdata: std_logic_vector(3 downto 0) := (others => '0');

begin

	sel <= to_integer(unsigned(ipb_in.ipb_addr(ADDR_WIDTH - 1 downto 0)));

	process(clk)
	begin
		if rising_edge(clk) then
			rdata <= ram(sel); -- Order of statements is important to infer read-first RAM!
			if ipb_in.ipb_strobe='1' and ipb_in.ipb_write='1' then
				ram(sel) := wdata;
			end if;
			ack <= ipb_in.ipb_strobe and not ack;
		end if;
	end process;
	
   ipb_out.ipb_rdata <= X"0000000" & rdata;
   wdata <= ipb_in.ipb_wdata(3 downto 0);
	ipb_out.ipb_ack <= ack;
	ipb_out.ipb_err <= '0';
	
	rsel <= to_integer(unsigned(addr));
	
	process(rclk)
	begin
		if rising_edge(rclk) then
			q <= ram(rsel); -- Order of statements is important to infer read-first RAM!
			if we = '1' then
				ram(rsel) := d;
			end if;
		end if;
	end process;

end rtl;
