-- wbone_dpram_frame_init.vhd
--
-- R. Staley, May 2019. Added initialisation of RAM with example WIB data

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.ipbus.all;

entity wbone_dpram_data_init is
	generic(
		ADDR_WIDTH: natural
	);
	port(
		clk: in std_logic;
		rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		rclk: in std_logic;
		we: in std_logic := '0';
		d: in std_logic_vector(31 downto 0) := (others => '0');
		q: out std_logic_vector(31 downto 0);
		addr: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
	
end wbone_dpram_data_init;

architecture rtl of wbone_dpram_data_init is

	type ram_array is array(0 to 2 ** ADDR_WIDTH - 1 ) of std_logic_vector(31 downto 0); -- to allow initialisation from address 0
	
	shared variable ram: ram_array :=(
  X"000000BC" ,  X"00000000" ,  X"00000001" ,  X"00000002" ,  X"00000003" ,  X"00000004" ,  X"00000005" ,  X"00000006" ,  X"00000007" ,  X"00000008" ,
  X"00000009" ,  X"0000000A" ,  X"0000000B" ,  X"0000000C" ,  X"0000000D" ,  X"0000000E" ,  X"0000000F" ,  X"00000010" ,  X"00000011" ,  X"00000012" , 
  X"00000013" ,  X"00000014" ,  X"00000015" ,  X"00000016" ,  X"00000017" ,  X"00000018" ,  X"00000019" ,  X"0000001A" ,  X"0000001B" ,  X"0000001C" , 
  X"0000001D" ,  X"0000001E" ,  X"0000001F" ,  X"00000020" ,  X"00000021" ,  X"00000022" ,  X"00000023" ,  X"00000024" ,  X"00000025" ,  X"00000026" , 
  X"00000027" ,  X"00000028" ,  X"00000029" ,  X"0000002A" ,  X"0000002B" ,  X"0000002C" ,  X"0000002D" ,  X"0000002E" ,  X"0000002F" ,  X"00000030" , 
  X"00000031" ,  X"00000032" ,  X"00000033" ,  X"00000034" ,  X"00000035" ,  X"00000036" ,  X"00000037" ,  X"00000038" ,  X"00000039" ,  X"0000003A" , 
  X"0000003B" ,  X"0000003C" ,  X"0000003D" ,  X"0000003E" ,  X"0000003F" ,  X"00000040" ,  X"00000041" ,  X"00000042" ,  X"00000043" ,  X"00000044" , 
  X"00000045" ,  X"00000046" ,  X"00000047" ,  X"00000048" ,  X"00000049" ,  X"0000004A" ,  X"0000004B" ,  X"0000004C" ,  X"0000004D" ,  X"0000004E" , 
  X"0000004F" ,  X"00000050" ,  X"00000051" ,  X"00000052" ,  X"00000053" ,  X"00000054" ,  X"00000055" ,  X"00000056" ,  X"00000057" ,  X"00000058" , 
  X"00000059" ,  X"0000005A" ,  X"0000005B" ,  X"0000005C" ,  X"0000005D" ,  X"0000005E" ,  X"0000005F" ,  X"00000060" ,  X"00000061" ,  X"00000062" , 
  X"00000063" ,  X"00000064" ,  X"00000065" ,  X"00000066" ,  X"00000067" ,  X"00000068" ,  X"00000069" ,  X"0000006A" ,  X"0000006B" ,  X"0000006C" , 
  X"0000006D" ,  X"0000006E" ,  X"0000006F" ,  X"00000070" ,  X"00000071" ,  X"00000072" ,  X"00000073" ,  X"0FFFFFDC" ,  X"000000BC" ,  X"000000BC" , 
    others => X"000000BC" ); -- WIB IDLE

	
	signal sel, rsel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
	signal ack: std_logic;

begin

	sel <= to_integer(unsigned(ipb_in.ipb_addr(ADDR_WIDTH - 1 downto 0)));

	process(clk)
	begin
		if rising_edge(clk) then
			ipb_out.ipb_rdata <= ram(sel); -- Order of statements is important to infer read-first RAM!
			if ipb_in.ipb_strobe='1' and ipb_in.ipb_write='1' then
				ram(sel) := ipb_in.ipb_wdata;
			end if;
			ack <= ipb_in.ipb_strobe and not ack;
		end if;
	end process;
	
	ipb_out.ipb_ack <= ack;
	ipb_out.ipb_err <= '0';
	
	rsel <= to_integer(unsigned(addr));
	
	process(rclk)
	begin
		if rising_edge(rclk) then
			q <= ram(rsel); -- Order of statements is important to infer read-first RAM!
			if we = '1' then
				ram(rsel) := d;
			end if;
		end if;
	end process;

end rtl;
