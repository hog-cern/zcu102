--Read stimuli from an input text file.
--The file shall have the following columns:
--  rst en start loop load load_val reps inhibit
--   1   1   1     1    1     10     16    32

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity sim_read_file is
  generic(
    pFILE : string := "./test.txt"
    );
  port(
    --sim_read_file ports
    iCLK        : in  std_logic := '0';
    iRST        : in  std_logic := '1';
    iEN         : in  std_logic := '0';
    oDATA_COUNT : out integer   := -1;
    oEOF        : out std_logic := '0';
    oINHIBIT    : out std_logic := '0';

    --DUT ports
    oSTART    : out std_logic                     := '0';
    oRST      : out std_logic                     := '0';
    oEN       : out std_logic                     := '0';
    oLOOP     : out std_logic                     := '0';
    oLOAD     : out std_logic                     := '0';
    oLOAD_VAL : out std_logic_vector(9 downto 0)  := (others => '0');
    oREPS     : out std_logic_vector(15 downto 0) := (others => '0')
    );
end entity sim_read_file;

architecture behav of sim_read_file is
  --File to be opened 
  file fTestVector   : text open read_mode is pFILE;
  signal sRowGood    : boolean;
  signal sInhibit    : std_logic := '0';
  signal sInhibitCnt : natural   := 0;
  signal sInhibitVal : natural   := 0;
  --signal sValSpace : character;

begin
  oINHIBIT <= sInhibit;

--!@brief Read file content line by line, synchronous to iCLK
  READ_PROC : process(iRST, iCLK, iEN)
    variable vRow        : line;
    variable vRowCount   : integer := -1;
    variable vRst        : natural := 0;
    variable vEn         : natural := 0;
    variable vStart      : natural := 0;
    variable vLoop       : natural := 0;
    variable vLoad       : natural := 0;
    variable vLoadVal    : natural := 0;
    variable vReps       : natural := 0;
    variable vInhibit    : natural := 0;
    variable vInhibitCnt : natural := 0;
  begin
    if(iRST = '1') then
      vRowCount   := 0;
      oEOF        <= '0';
      sInhibit    <= '0';
      vInhibitCnt := 0;
    elsif(rising_edge(iCLK)) then
      if(iEN = '1') then
        if (vInhibitCnt < vInhibit) then
          sInhibit    <= '1';
          vInhibitCnt := vInhibitCnt + 1;
        else  --inhibit
          vInhibitCnt := 0;
          --Read from input file in "vRow" variable
          if (not endfile(fTestVector)) then
            vRowCount := vRowCount + 1;
            readline(fTestVector, vRow);
            oEOF      <= '0';
            sInhibit  <= '0';
            --if (vRow'length > 0) then
            if (vRow.all(1) /= '-') then        --Line is not a comment
              --Read columns from vRow variable
              read(vRow, vRst);
              read(vRow, vEn);
              read(vRow, vStart);
              read(vRow, vLoop);
              read(vRow, vLoad);
              read(vRow, vLoadVal);
              read(vRow, vReps);
              read(vRow, vInhibit);
              assert false
                report "[DBUG] Values:"
                & integer'image(vRst) & " "
                & integer'image(vEn) & " "
                & integer'image(vStart) & " "
                & integer'image(vLoop) & " "
                & integer'image(vLoad) & " "
                & integer'image(vLoadVal) & " "
                & integer'image(vReps) & " "
                & integer'image(vInhibit) & " "
                severity note;
            end if; --vRow.all
          else  --endfile
            oEOF <= '1';
            assert false
              report "[NOTE] Reached end of file"
              severity failure;
          end if;  --endfile
          --Assign to the output ports
          oRST        <= std_logic(to_unsigned(vRst, 1)(0));
          oEN         <= std_logic(to_unsigned(vEn, 1)(0));
          oSTART      <= std_logic(to_unsigned(vStart, 1)(0));
          oLOOP       <= std_logic(to_unsigned(vLoop, 1)(0));
          oLOAD       <= std_logic(to_unsigned(vLoad, 1)(0));
          oLOAD_VAL   <= conv_std_logic_vector(vLoadVal, 10);
          oREPS       <= conv_std_logic_vector(vReps, 16);
          oDATA_COUNT <= vRowCount;
        end if;  --inhibit
      end if;  --EN
    end if;  --rising_edge
  end process READ_PROC;

end architecture behav;
